MVC.View = class View {
  constructor(elem){
    this.eventHandler()
    this.elem = elem
    this.eventHandlerDelete()
  }
  eventHandler(){
    document.body.addEventListener("onLoadData", (event) => {
      this.updateView(event.detail)
    })
  }
  eventHandlerDelete(){
    document.body.addEventListener("onLoadData", (event) => {
      this.deleteView(event.detail)
    })
  }

  notify(data){
    const onLoadDataEvent = new CustomEvent("onLoadData",{ detail:data, bubbles:true})
    this.elem.dispatchEvent(onLoadDataEvent)
  }
  postView(){
    let title = document.getElementById('title').value
    let author = document.getElementById('author').value
    let keywords = document.getElementById('keywords').value
    let article = document.getElementById('article').value
    let data = {
      "title": title,
      "author": author,
      "keywords": keywords,
      "article": article
    }
    if(title != "" && author != "" && keywords != "" && article != ""){
      alert('Se posteó Correctamente')
      return data
    }

  }
  updateView(datos){
    let listItem = document.getElementById('posts')
    for (var i = 0; i < datos.length; i++) {
      listItem.insertAdjacentHTML("beforeend",`
        <article class="col-md-12 article" id="list-item">
          <h1 class="aarticle">${datos[i].title}</h1>
          <h4 id="author" class="text-muted aauthor">${datos[i].author}</h4>
          <small id="keywords" class="akeywords">${datos[i].keywords}</small>
          <p id="article" class="articulo aarticle">${datos[i].article}</p>
        </article>
        `)
    }
  }

  deleteView(datos){
    let datalist = document.getElementById('delete')
    for (var i = 0; i < datos.length; i++) {
      datalist.insertAdjacentHTML("beforeend",`
        <option value="${datos[i].id}">
        `)
    }
  }
}

// let btnDelete = document.querySelectorAll('[data-delete]')
// function deletePost(event) {
//   event.preventDefault() // Impedir el salto de desplazamiento de página en el ancla
//   // Eliminar post
//   if( confirm(`Really Delete this post? ${this.dataset.delete} ${this.dataset.id}`) ) {
//     fetch(`http://localhost:8080/api/blog/${this.dataset.id}`, {
//       method: 'DELETE',
//     })
//       .then(function(response) {
//       })
//       .then(function(data) {
//         console.log(`data = ${data}`)
//       })
//       .catch(function(err) {
//         console.error(err)
//       })
//   }
// }
//
// // Asignar evento al botón
// [].forEach.call(btnDelete, function(btn) {
//   btn.addEventListener("click", deletePost, false)
// })
