MVC.Controller = class Controller {
  constructor(props){
    this.eventHandler()
    this.model = new props.model(props.endpoint)
    this.view = new props.view(props.contentElement)
    this.eventHandlerPost()
    this.eventHandlerButtonDelete()
  }
  eventHandler(){
    document.body.addEventListener("onloadApp", (event) => {
      this.getData()
    })
  }


  eventHandlerPost(){
    let submit = document.getElementById('submit')
    submit.addEventListener("click", (event) => {
      this.postData()
    })
  }
  eventHandlerButtonDelete(){
    let submit = document.querySelector('#superdelete')
    submit.addEventListener("click", (event) => {
      this.deleteData()
      window.location.reload(true)
    })
  }

  getData(){
    this.model.getBlog()
      .then(data => {
        this.view.notify(data)
      })
      .catch(console.log)
  }
  postData(){
    let object = this.view.postView()
    this.model.postBlog(object)
  }
  deleteData(){
    let id = document.querySelector('[list=delete]').value
    console.log(id);
    this.model.deleteBlog(id)
  }

}
