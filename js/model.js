MVC.Model = class Model {
  constructor(endpoint){
    this.endpoint = endpoint
    this.modelData = {}
  }
  getBlog(){
    return fetch(`${this.endpoint}`)
            .then(resp => {
              if (resp.ok) {
                return resp.json()
              }
              return Error("No se pudieron obtener los datos")
            })
            .then(data => {
              this.setModelo(data)
              return data
            })
  }
  searchBlog(blogId){
    this.blogId = blogId
    return fetch(`${this.endpoint}/${this.blogId}`)
            .then(resp => {
              if (resp.ok) {
                return resp.json()
              }
              return Error("No se pudieron obtener los datos")
            })
            .then(data => {
              this.setModelo(data)
              return data
            })
  }
  postBlog(data){
    return fetch(`${this.endpoint}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: JSON.stringify(data),
      cache:'no-cache'
    })
      .then(function(response) {
      })
      .then(function(data) {
        // console.log(`data = ${data}`)
      })
      .catch(function(err) {
        console.error(err)
      })
  }

  deleteBlog(data){
    return fetch(`${this.endpoint}/${data}`, {
      method: 'DELETE',
    })
      .then(function(response) {
      })
      .then(function(data) {
        console.log(`data = ${data}`)
      })
      .catch(function(err) {
        console.error(err)
      })
  }


  setModelo(data){
    this.modelData = data
  }
}
